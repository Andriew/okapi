var express = require('express');
var util = require('util');
var bodyParser = require('body-parser');
var fs = require('fs');
//var ioClient = require('socket.io-client')('http://127.0.0.1:3000'); //server address
//var ioClient = require('socket.io-client')('http://192.168.10.20:3000'); //server address
var Serwer = 'http://150.254.145.137:3000/uploads/'; //server address
var path = require('path');

var express = require('express');

var Downloads = require("./lib/Downloads");
var request = require('request');
//var Serwer = 'http://127.0.0.1:3000/uploads/'; //server address
var Serwer = 'http://150.254.145.137:3000/uploads/'; //server address
//var Serwer = 'http://192.168.10.20:3000/uploads/'; //server address

var app = express();
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', function(req, res) {

  /*
    var Download = new Downloads();
    Download.getIPAddress(function(err, addresses) {
        Download.getInfo(addresses[0], function(err, info) {
*/

            var Download = new Downloads();
            Download.getIPAddress(function(err, addresses) {
                Download.getInfo(addresses[0], function(err, info) {
                    //console.log(addresses[0]);
                    //console.log(info[0].OneScreen);

                    if (info.length != 0) {
                        if (info[0].OneScreen == 0) {
                            Download.DownloadToDevice(addresses[0], function(err, files, dir, maxDur, StartTemplate, StopTemplate) {
                                var time;
                                var FileToScreen1 = [];
                                var FileToScreen2 = [];
                                var MaxDur = 0;
                                for (var i = 0; i < files.length; i++) {
                                    time = "" + files[i].FileDuration;
                                    time = time.split(":");
                                    files[i].FileDuration = time[0] * 3600 + time[1] * 60 + time[2] * 1;
                                    files[i].Name = dir + files[i].Name;

                                    if (files[i].Screen == 1) {
                                        FileToScreen1.push(files[i]);
                                    }
                                    if (files[i].Screen == 2) {
                                        FileToScreen2.push(files[i]);
                                    }
                                }
                                MaxDur = maxDur.split(":");
                                maxDur = MaxDur[0] * 3600 + MaxDur[1] * 60 + MaxDur[2] * 1;

                                res.render('index', {
                                    maxDur: maxDur,
                                    FileToScreen1: FileToScreen1,
                                    FileToScreen2: FileToScreen2,
                                    StartTemplate: StartTemplate,
                                    StopTemplate: StopTemplate
                                });
                            });
                        } else {
                            Download.DownloadToDevice(addresses[0], function(err, files, dir, maxDur, StartTemplate, StopTemplate) {
                                var time;
                                var FileToScreen1 = [];
                                var MaxDur = 0;
                                //console.log(files.length);
                                for (var i = 0; i < files.length; i++) {
                                    time = "" + files[i].FileDuration;
                                    time = time.split(":");
                                    files[i].FileDuration = time[0] * 3600 + time[1] * 60 + time[2] * 1;
                                    files[i].Name = dir + files[i].Name;
                                    if (files[i].Screen == 1) {
                                        FileToScreen1.push(files[i]);
                                    }
                                }
                                MaxDur = maxDur.split(":");
                                maxDur = MaxDur[0] * 3600 + MaxDur[1] * 60 + MaxDur[2] * 1;

                                res.render('indexOfOne', {
                                    //inna podstrona
                                    maxDur: maxDur,
                                    FileToScreen1: FileToScreen1,
                                    StartTemplate: StartTemplate,
                                    StopTemplate: StopTemplate
                                });
                            });
                        }
                    } else {
                        res.render('tempSite');
                    }
                })
            });
        //});
    //});
});
app.post('/getBreakingNews', function(req, res) {
    var download = new Downloads();
    download.getIPAddress(function(err, ip) {
        download.getBreakingNews(ip[0], null, function(err, breakingNews) {
            if (err) return err;
            //console.log(breakingNews);
            if (breakingNews.length != 0) {
                res.send([breakingNews[0].Content, breakingNews[0].Category, breakingNews[0].StartTime, breakingNews[0].EndTime]);
            } else
                res.send('none');
        });
    });
});

app.post('/checkOnline', function(req, res) {
    var key = req.body.key;
    //console.log(key);
    if (key == 'checkOnline') {
        res.writeHead(200, "ONLINE", {
            'Content-Type': 'text/plain'
        });
        res.end();
    } else {
        res.writeHead(403, "OFFLINE", {
            'Content-Type': 'text/plain'
        });
        res.end();
    }
});

app.listen(7474, function() {
    console.log("Started on PORT 7474");
});
