var mysql = require('mysql');
var dbconfig = require('../config/database');
var db = mysql.createConnection(dbconfig.connection);
var connection = db.query('USE ' + dbconfig.database);
var fs = require('fs');

var request = require('request');
//var Serwer = 'http://127.0.0.1:3000/uploads/';
//var Serwer = 'http://192.168.10.20:3000/uploads/';
var Serwer = 'http://150.254.145.137:3000/uploads/'; //server address
var dir;
var port = ":7474";

function Downloads() {

}

function Downloads() {}

Downloads.prototype.getInfo = function(address, cb) {

    var IPAddress = address + ':7474';
    db.query('SELECT T.OneScreen, TD.ID FROM TemplateToDevice TD JOIN Devices D ON D.ID = TD.DeviceID JOIN Templates T ON T.ID = TD.TemplateID WHERE D.IPAddress = ? AND T.EndDate > NOW() ', [
            IPAddress
        ],
        function(err, info) {
            if (err) return cb(err);
            cb(null, info);
        });
};

Downloads.prototype.DownloadToDevice = function(address, cb) {
    var IPAddress = address + ':7474';

    db.query('SELECT Templates.ID, Templates.Name, Templates.DurationTime, Templates.StartDate, Templates.EndDate FROM Templates INNER JOIN TemplateToDevice ON Templates.ID = TemplateToDevice.TemplateID INNER JOIN Devices ON Devices.ID = TemplateToDevice.DeviceID AND Devices.IPAddress = ?', [
            IPAddress
        ],
        function(err, files) {
            if (err) throw err;
            else {
                var StartTemplate = files[0].StartDate;
                var StopTemplate = files[0].EndDate;
                var TemplateId = files[0].ID;
                var NameT = files[0].Name;
                var maxDur = files[0].DurationTime;
                dir = './public/uploads/' + NameT + '/';
                if (!fs.existsSync(dir)) {
                    fs.mkdirSync(dir);
                }
            }

    db.query('SELECT FT.ID, F.Name, FT.FileDuration, FT.Screen FROM Files F INNER JOIN FileToTemplate FT ON FT.FileID = F.ID AND FT.TemplateID = ? ORDER BY FT.ID ', [
            TemplateId
        ],
        function(err, files) {
            if (err) throw err;
            else {
                var Files = files
                for (var i in Files) {
                    if (!fs.existsSync(dir + Files[i].Name)) {
                        request.get(Serwer + Files[i].Name).on('response', function(response) {
                                console.log("pobrano");
                            })
                            .pipe(fs.createWriteStream(dir + Files[i].Name));
                    }
                }
              }


                        dir = '/uploads/' + NameT + '/';
                        cb(null, Files, dir, maxDur, StartTemplate, StopTemplate);

                    });
                    });
            }



Downloads.prototype.getIPAddress = function(cb) {
    var os = require('os');
    var interfaces = os.networkInterfaces();
    var addresses = [];
    for (var k in interfaces) {
        for (var k2 in interfaces[k]) {
            var address = interfaces[k][k2];
            if (address.family === 'IPv4' && !address.internal) {
                addresses.push(address.address);
            }
        }
    }
    cb(null, addresses);
};




Downloads.prototype.getBreakingNews = function(ip, next, cb) {
    var ip = ip + port;
    db.query('SELECT * FROM BreakingNews INNER JOIN Devices on BreakingNews.ID=Devices.BreakingNewsID AND Devices.IPAddress= ?', [ip],
        function(err, result) {
            if (err) return cb(err);
            cb(null, result);
        });
};

module.exports = Downloads;
