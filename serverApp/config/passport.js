// config/passport.js

// load all the things we need
var LocalStrategy = require('passport-local').Strategy;

// load up the user model
var mysql = require('mysql');
var bcrypt = require('bcrypt-nodejs');
var dbconfig = require('./database');
var connection = mysql.createConnection(dbconfig.connection);

connection.query('USE ' + dbconfig.database);
// expose this function to our app using module.exports
module.exports = function(passport) {
    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        var sessionUser = { ID: user.ID, Login: user.Login, role: user.Role };
        done(null, sessionUser);
    });

    // used to deserialize the user
    passport.deserializeUser(function(sessionUser, done) {
        connection.query("SELECT * FROM Users WHERE id = ? ", [sessionUser.ID],
            function(err, rows) {
                done(err, sessionUser);
            });
    });
    /*
    passport.deserializeUser(function(id, done) {
        connection.query("SELECT * FROM Users WHERE id = ? ", [id],
            function(err, rows) {
                done(err, rows[0]);
            });
    });
    */
    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-addUser', new LocalStrategy({
            // by default, local strategy uses Login and password, we will override with email
            usernameField: 'login',
            passwordField: 'passVal',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {
            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            connection.query("SELECT * FROM Users WHERE Login = ?", [username],
                function(err, rows) {
                    if (err) {
                        return done(err);
                    }
                    if (rows.length) {
                        return done(null, false, req.flash('message', 'That Login is already taken.'));
                    } else {
                        // if there is no user with that Login
                        // create the user
                        var newUserMysql = {
                            Login: username,
                            Password: bcrypt.hashSync(password, null, null), // use the generateHash function in our user model
                            Email: req.body.email,
                            Role: req.body.role,
                            Block: 0
                        };

                        var insertQuery = "INSERT INTO Users ( Login, Password, Email, Role, Block ) values (?,?,?,?,?)";
                        connection.query(insertQuery, [
                                newUserMysql.Login,
                                newUserMysql.Password,
                                newUserMysql.Email,
                                newUserMysql.Role,
                                newUserMysql.Block
                            ],
                            function(err, rows) {
                                if (err) {
                                    return done(err);
                                } else {
                                    return done(null, true, req.flash('message', 'Success!'));
                                }
                            });
                    }
                });
        }));

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-login', new LocalStrategy({
            // by default, local strategy uses Login and password, we will override with email
            usernameField: 'login',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) { // callback with email and password from our form
            connection.query("SELECT * FROM Users WHERE Login = ?", [username],
                function(err, rows) {
                    if (err) return done(err);
                    if (!rows.length) {
                        // req.flash is the way to set flashdata using connect-flash
                        return done(null, false, req.flash('loginMessage', 'No user found.'));
                    }
                    // if the user is found but the password is wrong
                    if (!bcrypt.compareSync(password, rows[0].Password)) {
                        // create the loginMessage and save it to session as flashdata
                        return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
                    }
                    //if user is block
                    if (rows[0].Block) {
                        // create the loginMessage and save it to session as flashdata
                        return done(null, false, req.flash('loginMessage', 'User hasa been blocked'));
                    }

                    // all is well, return successful user
                    return done(null, rows[0]);
                });
        }));
};
