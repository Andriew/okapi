var mysql = require('mysql');
var dbconfig = require('../config/database');
var db = mysql.createConnection(dbconfig.connection);
var connection = db.query('USE ' + dbconfig.database);
var jsonfile = require('jsonfile');

function Templates() {
    //construktor
    //console.log('konsturktor');
}

Templates.prototype.uploadDuration = function(Duration, IDTemp) {

    db.query('UPDATE Templates SET DurationTime = ? WHERE ID = ?', [Duration, IDTemp],
        function selectCb(err, res) {
            if (err) throw err;
            //console.log("duration template updated"); // res.send('success');
        });
};

//usuwa wszystkie takie same elementy
Templates.prototype.DeleteFromScreen1 = function(FileID1) {

    db.query('DELETE FROM FileToTemplate WHERE FileID = ? AND Screen = 1', [
            FileID1
        ],
        function(err, files) {
            if (err) return cb(err);
            //console.log("delete " + FileID1);
        });
    //
};

Templates.prototype.DeleteFromScreen2 = function(FileID2) {

    db.query('DELETE FROM FileToTemplate WHERE FileID = ? AND Screen = 2', [
            FileID2
        ],
        function(err, files) {
            if (err) return cb(err);
            //console.log(files);
        });
    //
};

Templates.prototype.DisplayFilesFromScreen1 = function(TempID, cb) {

    db.query('SELECT FileToTemplate.ID, FileToTemplate.FileID, FileToTemplate.FileDuration, Files.Name FROM FileToTemplate JOIN Files ON Files.ID = FileToTemplate.FileID AND FileToTemplate.TemplateID = ' + TempID + ' WHERE FileToTemplate.Screen = 1 ORDER BY FileToTemplate.ID',
        function(err, files) {
            if (err) return cb(err);
            //console.log(files);
            cb(null, files);
        });
    //
};


Templates.prototype.DisplayFilesFromScreen2 = function(TempID, cb) {

    db.query('SELECT FileToTemplate.ID, FileToTemplate.FileID, FileToTemplate.FileDuration, Files.Name FROM FileToTemplate JOIN Files ON Files.ID = FileToTemplate.FileID AND FileToTemplate.TemplateID = ? WHERE FileToTemplate.Screen = 2 ORDER BY FileToTemplate.ID', [
            TempID
        ],
        function(err, files) {
            if (err) return cb(err);
            //console.log(files);
            cb(null, files);
        });
    //
};

Templates.prototype.addFileToScreen1 = function(Screen1, IDFile1, IDTemp, Time1) {

    if (Screen1 != "") {
        //console.log(TempFile);
        var ScreenNumber = 1;
        db.query('insert into  FileToTemplate (FileID ,TemplateID, FileDuration, Screen) VALUES (?,?,?,?)', [
                IDFile1,
                IDTemp,
                Time1,
                ScreenNumber
            ],
            function selectCb(err, res) {
                if (err) throw err;
                else {
                    //console.log("templete addedT2"); // res.send('success');
                    //console.log("ekran 2 " + req.body.ekran_2);
                }
            });

        //
        //console.log("nice, good adding file to screen1");
    } else {
        //console.log("FAIL on adding file in screen1");
    }
};

Templates.prototype.addFileToScreen2 = function(Screen2, IDFile2, IDTemp, Time2) {

    if (Screen2 != "") {
        //console.log(TempFile);
        var ScreenNumber = 2;
        db.query('insert into  FileToTemplate (FileID ,TemplateID, FileDuration, Screen) VALUES (?,?,?,?)', [
                IDFile2,
                IDTemp,
                Time2,
                ScreenNumber
            ],
            function selectCb(err, res) {
                if (err) throw err;
                else {
                    //console.log("templete addedT2"); // res.send('success');
                    //console.log("ekran 2 " + req.body.ekran_2);
                }
            });
        //console.log("nice, good adding file to screen2");
    } else {
        //console.log("FAIL on adding file in screen2");
    }
};

Templates.prototype.updateTemplateOnDevice = function(DeviceID, TemplateID) {
    db.query('insert into TemplateToDevice (TemplateID ,DeviceID) values (?, ?)', [TemplateID, DeviceID],
          function(err) {
            if (err) throw err;
          });
};

Templates.prototype.getAll = function(cb) {
    db.query('SELECT * FROM Templates', function(err, devices) {
        if (err) return cb(err);
        cb(null, devices);
    });
};

Templates.prototype.deleteTemplate = function(id) {
    db.query('DELETE FROM TemplateToDevice WHERE TemplateID = ?', [id],
        function(err, result) {
            if (err) throw err;
        });

    db.query('DELETE FROM FileToTemplate WHERE TemplateID= ?', [id],
        function(err, result) {
            if (err) throw err;
        });

    db.query('DELETE FROM Templates WHERE id= ?', [id],
        function(err, result) {
            if (err) throw err;
        });
};

Templates.prototype.createTemplate = function(NAME, time, oneScreen) {

    db.query('insert into Templates (Name ,CreationDate, OneScreen) values (?,?,?);',
    [
      NAME,
      time,
      oneScreen
    ],
        function selectCb(err, res, fields) {
            if (err) throw err;
            else {} //console.log("added"); // res.send('success');
        });

};

Templates.prototype.createJSON = function(cb) {
    var file = './Files.json';
    db.query('SELECT * FROM Files', function(err, files) {
        if (err) throw err;
        //console.log("files2 " + files);
        //files = rows;
        jsonfile.writeFile(file, files, function(err) {
            if (err) console.error(err);
        });
        cb(null, files)
    });
};

Templates.prototype.drawFile = function(cb) {
    db.query('SELECT * FROM Files', function(err, files) {
        if (err) throw err;
        else {
            //files = rows;
            cb(null, files);
        }
    });
};



Templates.prototype.staredTemplate = function(NAME, cb) {
    var dateFromDB;
    var ID;
    var oneScreen;
    var StartTime;
    var EndTime;
    db.query('SELECT * FROM Templates WHERE Name = ? ',
    [
      NAME
    ], function(err, rows) {
        if (err) throw err;
        else {
                    dateFromDB = rows[0].CreationDate;
                    ID = rows[0].ID;
                    oneScreen = rows[0].OneScreen;
                    StartTime = rows[0].StartDate;
                    EndTime = rows[0].EndDate;
                    //console.log("test2 " + dateFromDB);
        }
        cb(null, dateFromDB, ID, oneScreen, StartTime, EndTime);
    });
};


//raczej del
Templates.prototype.fileTotemplate = function(TempID, ekran1, ekran2, time1, time2) {
    var files;
    var FileID;
    var duration;
    var Screen;
    var TempFile;

    db.query('SELECT * FROM Files', function(err, rows) {
        if (err) throw err;
        files = rows;
        db.query('SELECT * FROM FileToTemplate ', function(err, temp) {
            if (err) throw err;
            TempFile = temp;
            //console.log("lol " + TempFile.length);
            //console.log(TempID);


            if (ekran1 != "") {
                //console.log(TempFile);
                Screen = 1;
                for (var i = 0; i < rows.length; i++) {
                    if (rows[i].Name == ekran1) {
                        FileID = rows[i].ID;
                        duration = time1;
                        //console.log("FileID1 " + FileID);
                        //console.log("Duration1 " + duration);
                        //console.log("TempID1 " + TempID);
                    }
                }
                db.query('insert into  FileToTemplate (FileID ,TemplateID, FileDuration, Screen) VALUES ("' + FileID + '", "' + TempID + '", "' + duration + '", "' + Screen + '");',
                    function selectCb(err, res) {
                        if (err) throw err;
                        else {
                            //console.log("templete addedT2"); // res.send('success');
                            //console.log("ekran 2 " + req.body.ekran_2);
                        }
                    });
            } else
                //console.log("screen 1 empty");

            if (ekran2 != "") {
                Screen = 2;
                for (var i = 0; i < rows.length; i++) {
                    if (rows[i].Name == ekran2) {
                        FileID = rows[i].ID;
                        duration = time2;
                        //console.log("FileID2 " + FileID);
                        //console.log("Duration2 " + duration);
                        //console.log("TempID2 " + TempID);
                    }
                }

                db.query('insert into  FileToTemplate (FileID ,TemplateID, FileDuration, Screen) VALUES ("' + FileID + '", "' + TempID + '", "' + duration + '", "' + Screen + '");',
                    function selectCb(err, res, fields) {
                        if (err) throw err;
                        else {
                            //console.log("templete addedT3"); // res.send('success');
                        }
                    });
            } else {}
                //console.log("screen 2 empty");

        });
    });
};

Templates.prototype.TemplateInfo = function(NAME, cb) {

    var tempID;
    var duration;
    var time;

    //var NAME;
    //console.log(NAME);
    db.query('SELECT * FROM Templates WHERE ?', [{
        Name: NAME
    }], function(err, rows) {
        if (err) throw err;
        else {
            //console.log("testyXYZ " + dateFromDB[0].ID);
            //console.log("testy123 " + rows[0].Name);
            //console.log(NAME);
            //console.log(dateFromDB);
            //console.log(rows);

            time = rows[0].CreationDate;

            TempID = rows[0].ID;

            //console.log("TempIDY " + TempID);
        }
        cb(null, TempID);
    });
};

Templates.prototype.TemplateUpdate = function(NAME, DataBegin, DataEnd, Author) {
    //add HowManyScreens
    db.query('UPDATE Templates SET ? WHERE ?', [{
        Author: Author,
        StartDate: DataBegin,
        EndDate: DataEnd
    }, {
        Name: NAME
    }], function selectCb(err, res, fields) {
        if (err) throw err;
        else {
            //console.log("templete updated"); // res.send('success');
        }
    });

};

Templates.prototype.getTemplateName = function(deviceID, next, cb) {
    db.query('SELECT Templates.Name FROM Templates INNER JOIN TemplateToDevice ON Templates.ID = TemplateToDevice.TemplateID INNER JOIN Devices ON TemplateToDevice.DeviceID = Devices.ID AND Devices.ID = ?', [deviceID],
        function(err, result) {
            if (err) return cb(err);
            cb(null, result);
        });
};


module.exports = Templates;
