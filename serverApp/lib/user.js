var mysql = require('mysql');
var bcrypt = require('bcrypt-nodejs');
var dbconfig = require('../config/database');
var db = mysql.createConnection(dbconfig.connection);
var connection = db.query('USE ' + dbconfig.database);

function User() {
    //construktor
    //console.log('konsturktor');
}

User.prototype.getAll = function(cb) {
    db.query('SELECT * FROM Users', function(err, users) {
        if (err) return cb(err);
        cb(null, users);
    });
};

User.prototype.blockUser = function(id, blockVal) {
    blockVal = blockVal == 1 ? 0 : 1;
    db.query('UPDATE Users SET Block = ? Where ID = ?', [blockVal, id],
        function(err, result) {
            if (err) throw err;
        });
};

User.prototype.deleteUser = function(id) {
    db.query('DELETE FROM Users WHERE id= ?', [id],
        function(err, result) {
            if (err) throw err;
        });
};

User.prototype.resetPassword = function(id, passVal) {
    //console.log('passval: ' + passVal);
    //console.log('hashpassval: ' + bcrypt.hashSync(passVal, null, null));
    db.query('UPDATE Users SET Password = ? Where ID = ?', [bcrypt.hashSync(passVal, null, null), id],
        function(err, result) {
            if (err) throw err;
        });
};

User.prototype.getNameByID = function(id, next, cb) {
    db.query('SELECT Login FROM Users WHERE ID = ?', [id], function(err, user) {
        if (err) return cb(err);
        cb(null, user);
    });
};

//console.log('Changed ' + result.changedRows + ' rows');

User.prototype.changeRole = function(userID, roleVal) {
    db.query('UPDATE Users SET Role = ? Where ID = ?', [roleVal, userID],
        function(err, result) {
            if (err) throw err;
        });
};


module.exports = User;
