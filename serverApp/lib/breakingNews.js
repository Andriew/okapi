var mysql = require('mysql');
var request = require('request');
var dbconfig = require('../config/database');
var db = mysql.createConnection(dbconfig.connection);
var connection = db.query('USE ' + dbconfig.database);

function BreakingNews() {
  //construktor
  //console.log('konsturktor');
}

BreakingNews.prototype.getBreakingNews = function(deviceID, next, cb) {
  db.query('SELECT * FROM BreakingNews INNER JOIN Devices on BreakingNews.ID=Devices.BreakingNewsID AND Devices.ID= ?', [deviceID],
      function(err, result) {
          if (err) return cb(err);
          cb(null, result);
      });
};

BreakingNews.prototype.changeBreakingNews = function(deviceID, IDBreakingNews, ContentBreakingNews, CategoryBreakingNews, StartTime, EndTime) {
  db.query('UPDATE BreakingNews SET Content = ?, Category = ?, StartTime = ?, EndTime = ? WHERE ID = ? ', [ContentBreakingNews, CategoryBreakingNews, StartTime, EndTime, IDBreakingNews],
      function(err, result) {
          if (err) return err;
      });
  db.query('UPDATE Devices SET BreakingNewsID = ? WHERE ID = ? ', [IDBreakingNews, deviceID],
      function(err, result) {
          if (err) return err;
      });
};

BreakingNews.prototype.getCategoryBreakingNews = function(breakingNewsID, next, cb) {
  db.query('SELECT * FROM BreakingNews WHERE ID = ? ', [breakingNewsID],
      function(err, result) {
          if (err) return cb(err);
          cb(null, result);
      });
};

BreakingNews.prototype.removeBreakingNewsFromDevice = function(deviceID) {
  db.query('UPDATE Devices SET BreakingNewsID = null WHERE ID = ? ', [deviceID],
      function(err, result) {
          if (err) return err;
      });
};

BreakingNews.prototype.removeBreakingNewsFromDB = function(breakingNewsID) {
  db.query('UPDATE Devices SET BreakingNewsID = null WHERE BreakingNewsID = ? ', [breakingNewsID],
      function(err, result) {
          if (err) return err;
        db.query('DELETE FROM BreakingNews WHERE ID = ? ', [breakingNewsID],
            function(err, result) {
                if (err) return err;
            });
      });
};

BreakingNews.prototype.addNewBreakingNews = function(breakingNewsContent, breakingNewsCategory) {
  db.query('INSERT INTO BreakingNews (Content, Category) VALUES (?, ?)', [breakingNewsContent, breakingNewsCategory],
      function(err, result) {
          if (err) throw err;
      });
};

module.exports = BreakingNews;
