var mysql = require('mysql');
var dbconfig = require('../config/database');
var db = mysql.createConnection(dbconfig.connection);
var connection = db.query('USE ' + dbconfig.database);

var multer = require('multer');

var path2 = 'public/uploads/';
var fs = require('fs');

function Files() {
//construktor
//console.log('konsturktor');
}


Files.prototype.addFile = function(temp){

  var Split;
  var Path; // sciezka do pliku wykonywalnego i podzielenie go
  var Path1; // dodanie odpowiedniej sciezki zapisu pliku
  var Path2; // zamiana maku / na \
  var Type;
  var Ext;
  var Name;
  var Size;
  var Size2;
  var number; // konkretna wielksc
  var mode; // rodzaj wielkosc KB lub MB
  var stats;
  var fileSizeInBytes;


  if (temp == undefined)
  {

  } else {

    Split = temp.originalname.split(".");
    Ext = Split[Split.length-1].toLowerCase();
    var AvailableExtensions = ["jpg", "png", "mp4", "gif", "mp4"];
  }
  if (temp == undefined || AvailableExtensions.indexOf(Ext) == -1)
  {

          //console.log("fail");
  } else {
          //Split = temp.originalname.split(".");
          Name = temp.originalname.toLowerCase();
          //console.log('nazwa ' + Name);
          Path = process.argv[1].split("bin");
          //console.log(Path[0] + path2 + obiekt);
          Path1 = Path[0] + path2 + temp.originalname;
          Path2 = Path1.replace(/\\/g, "/")
          //console.log('sciezka ' + Path2);
          //console.log('nowy tekst ' + Path2);
          Type = temp.mimetype;
          //console.log('typ ' + Type);
          //Ext = Split[Split.length-1];
          //console.log('rozszerzenie ' + Ext);

          stats = fs.statSync(Path2);
          fileSizeInBytes = stats["size"];
          Size = fileSizeInBytes / 1048576;
          if (Size < 1) {
              Size2 = fileSizeInBytes / 1024;
              mode = 1;

          } else {
              Size2 = fileSizeInBytes / 1048576;
              mode = 2;
          }
          number = Size2.toFixed(3);
          if (mode == 1) {
              number = number + " KB";
          } else {
              number = number + " MB";
          }

          //console.log('wielkosc ' + number);

          db.query('insert into  Files (Name, Path, Type, Extension, Size) values ("' + Name + '", "' + Path2 + '", "' + Type + '", "' + Ext + '", "' + number + '");',
              function selectCb(err, res, fields) {
                  if (err) throw err;
                  else {} //console.log("added"); // res.send('success');
              });
      }
};

Files.prototype.deleteFile = function(FileName, FileId){
    var path = './public/uploads/';
    fs.unlinkSync(path + FileName);

    db.query('DELETE FROM FileToTemplate WHERE FileID= ?', [FileId],
        function(err, result) {
            if (err) throw err;
        });

    db.query('DELETE FROM Files WHERE Name= ?', [FileName],
        function(err, result) {
            if (err) throw err;
        });

};

module.exports = Files;
