var mysql = require('mysql');
var request = require('request');
var dbconfig = require('../config/database');
var db = mysql.createConnection(dbconfig.connection);
var connection = db.query('USE ' + dbconfig.database);

function Remote() {
    //construktor
    //console.log('konsturktor');
}

Remote.prototype.checkOnline = function(deviceID, ip) {
    var statusCode;
    request.post('http://' + ip + '/checkOnline', {
            form: {
                key: 'checkOnline'
            }
        },
        function(error, response, body) {
            if (!error && response.statusCode == 200) {
                statusCode = 200;
            } else {
                statusCode = 403;
            }
            db.query('UPDATE Devices SET StatusID = ( SELECT ID FROM DeviceStatus WHERE CodeStatus = ? ) WHERE ID = ?', [statusCode, deviceID],
                function(err, result) {
                    if (err) throw err;
                });
        });
};

Remote.prototype.getStatus = function(deviceID, res) {
    var statusCode;
    db.query('SELECT StatusID FROM Devices WHERE ID = ?', [deviceID],
        function(err, result) {
            if (err) throw err;
            res.send(result[0]);
        });
};

module.exports = Remote;
