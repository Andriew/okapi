var mysql = require('mysql');
var request = require('request');
var dbconfig = require('../config/database');
var db = mysql.createConnection(dbconfig.connection);
var connection = db.query('USE ' + dbconfig.database);

function Devices() {
  //construktor
}

Devices.prototype.getAll = function(cb) {
  db.query('SELECT * FROM Devices', function(err, devices) {
      if (err) return cb(err);
      cb(null, devices);
  });
};

Devices.prototype.getAdminsName = function(deviceID, next, cb) {
  db.query('SELECT Login FROM Users INNER JOIN UserToDevice ON UserToDevice.UserID=Users.ID inner JOIN Devices ON Devices.ID=UserToDevice.DeviceID AND Devices.ID = ?', [deviceID],
      function(err, result) {
          if (err) return cb(err);
          cb(null, result);
      });
};

Devices.prototype.getStatusCode = function(statusID, next, cb) {
  db.query('SELECT * FROM DeviceStatus WHERE ID= ?', [statusID],
      function(err, statusCode) {
          if (err) return cb(err);
          cb(null, statusCode);
      });
};

Devices.prototype.AddDevice = function(name, location, ip, deviceAdmin) {
  db.query('INSERT INTO Devices (Name, Localization, IPAddress, StatusID) VALUES (?, ?, ?, ?)', [name, location, ip, 2],
      function(err, result) {
        if (err) throw err;
        db.query('INSERT INTO UserToDevice ( UserID, DeviceID) VALUES (?, ?)', [deviceAdmin, result.insertId],
          function(err, result2) {
            if (err) throw err;
          });
      });
};

Devices.prototype.deleteDevice = function(id) {
  db.query('DELETE FROM TemplateToDevice WHERE DeviceID= ?', [id],
      function(err, result) {
          if (err) throw err;
      });

  db.query('DELETE FROM UserToDevice WHERE DeviceID= ?', [id],
      function(err, result) {
          if (err) throw err;
      });

  db.query('DELETE FROM Devices WHERE id= ?', [id],
      function(err, result) {
          if (err) throw err;
      });
};

Devices.prototype.addNewAdmin = function(deviceID, newAdminID, res) {
  db.query('SELECT * FROM UserToDevice WHERE UserID = ? AND DeviceID= ?', [newAdminID, deviceID],
      function(err, result) {
          if (err) throw err;
          if (result.length == 0) {
              db.query('INSERT INTO UserToDevice (UserID, DeviceID) VALUES (?, ?)', [newAdminID, deviceID],
                  function(err, result) {
                      if (err) throw err;
                  });
              res.send('OK');
          } else
              res.send('Error');
      });
};

Devices.prototype.removeAdmin = function(deviceID, adminID) {
  db.query('DELETE FROM UserToDevice WHERE UserID = ? AND DeviceID = ?', [adminID, deviceID],
      function(err, result) {
          if (err) throw err;
      });
};


Devices.prototype.changeIP = function(deviceID, ipAddress, res) {
  db.query('SELECT * FROM Devices WHERE IPAddress = ?', [ipAddress],
      function(err, result) {
          if (err) throw err;
          if (result.length == 0) {
              db.query('UPDATE Devices SET IPAddress = ? WHERE ID = ?', [ipAddress, deviceID],
                  function(err, result) {
                      if (err) throw err;
                  });
              res.send('OK');
          } else
              res.send('Error');
      });
};


module.exports = Devices;
