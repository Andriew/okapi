var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var session = require('express-session');
var flash = require('connect-flash');
var favicon = require('serve-favicon');

//routes
var routes = require('./routes/index');

var passport = require('passport');
require('./config/passport')(passport); // pass passport for configuration
var login = require('./routes/login')(passport);
var loginArea = require('./routes/loginArea')(passport);
var files = require('./routes/filesRoute');

//express start
var app = express();

app.locals.moment = require('moment');
app.locals.title = "OKAPI - Organizacja Kolportażu Aktualnych Paneli Informacyjnych";
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon(path.join(__dirname, 'public/images', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(morgan('dev')); // log every request to the console
app.use(cookieParser());
//app.use('/bootstrap', express.static('public/bootsrap'));
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(flash()); // use connect-flash for flash messages stored in session

// required for passport
app.use(session({
    secret: 'vidyapathaisalwaysrunning',
    resave: true,
    saveUninitialized: true
})); // session secret

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

app.use(function(req, res, next) {
    res.locals.session = req.session;
    next();
});

app.use('/', routes);
app.use('/login', login);
app.use('/loginArea', loginArea);
app.use('/files', files);

app.all('/*', function(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        res.render('login/');
    }
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
