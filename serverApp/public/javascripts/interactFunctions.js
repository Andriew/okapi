
  // target elements with the "draggable" class
  interact('.draggable')
    .draggable({
      // enable inertial throwing
      inertia: true,
      // keep the element within the area of it's parent
      restrict: {
        restriction: "parent",
        endOnly: true,
        elementRect: { top: 0, left: 0, bottom: -10, right: -10 }
      },
      // enable autoScroll
      autoScroll: true,

      // call this function on every dragmove event
      onmove: dragMoveListener,
      // call this function on every dragend event

    });

    function dragMoveListener (event) {
      var target = event.target,
          // keep the dragged position in the data-x/data-y attributes
          x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
          y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

      // translate the element
      target.style.webkitTransform =
      target.style.transform =
        'translate(' + x + 'px, ' + y + 'px)';

      // update the posiion attributes
      target.setAttribute('data-x', x);
      target.setAttribute('data-y', y);
    }

    // this is used later in the resizing and gesture demos
    window.dragMoveListener = dragMoveListener;

    interact('.dropzone').dropzone({
      // only accept elements matching this CSS selector
      //accept: '#yes-drop',
      // Require a 100% element overlap for a drop to be possible
      overlap: 1.0,

      // listen for drop related events:

      ondropactivate: function (event) {
        // add active dropzone feedback
        event.target.classList.add('drop-active');
      },
      ondragenter: function (event) {
        var draggableElement = event.relatedTarget,
            dropzoneElement = event.target;

        // feedback the possibility of a drop
        dropzoneElement.classList.add('drop-target');
        draggableElement.classList.add('can-drop');

      },
      ondragleave: function (event) {
        // remove the drop feedback style
        event.target.classList.remove('drop-target');
        event.relatedTarget.classList.remove('can-drop');

      },
      ondrop: function (event) {
        var name ="";

        if(event.relatedTarget.name == undefined){
          name = event.relatedTarget.src.split("/");
          name = name[4];
          //alert(name);
        } else {
          name = event.relatedTarget.name;
          //alert(name);
        }


          //alert(event.relatedTarget.name);
          //alert(event.target.id);
          //ogarnąc nazwe pod filmikami
          if(event.target.id == "outer-dropzone" ){
            document.getElementById('ekran1').value = name;
            document.getElementById('IDFile1').value = event.relatedTarget.id;
            //alert(event.relatedTarget.src);
            var vid = document.getElementById(event.relatedTarget.id);
            var vidDur = vid.duration;
            if(vidDur == undefined){
              vidDur = '';
              document.getElementById('Time1').value = vidDur;
            } else{
              document.getElementById('Time1').value = Math.round(vidDur);
            }
          }

          if(event.target.id == "inner-dropzone" )
            {
              document.getElementById('ekran2').value = name;
              document.getElementById('IDFile2').value = event.relatedTarget.id;

              var vid = document.getElementById(event.relatedTarget.id);
              var vidDur = vid.duration;
              if(vidDur == undefined){
                vidDur = '';
                document.getElementById('Time2').value = vidDur;
              } else{
                document.getElementById('Time2').value = Math.round(vidDur);
              }
            }

      },
      ondropdeactivate: function (event) {
        // remove active dropzone feedback

        event.target.classList.remove('drop-active');
        event.target.classList.remove('drop-target');
      }
    });
