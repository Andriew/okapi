var express = require('express');
var router = express.Router();
var mysql = require('mysql');



// =====================================
// Main Site ===========================
// =====================================
router.get('/', function(req, res, next) {

    res.render('index', {
        title: 'OKAPI'
    });
});

// =====================================
// LOGOUT ==============================
// =====================================
router.get('/logout', function(req, res) {
    req.logout();
    //console.log(req.session.passport.user);
    res.redirect('/login');
});

module.exports = router;
