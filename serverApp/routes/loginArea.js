var express = require('express');

var multer = require('multer');
var async = require('async');

var User = require('../lib/user');
var BreakingNews = require('../lib/breakingNews');
var Devices = require('../lib/devices');
var Remote = require('../lib/remote');
var Files = require('../lib/Files');

var jsonfile = require('jsonfile');
var fs = require('fs');
var Templates = require('../lib/Templates');

var mysql = require('mysql');
var dbconfig = require('../config/database');
var db = mysql.createConnection(dbconfig.connection);
var connection = db.query('USE ' + dbconfig.database);

var router = express.Router();

module.exports = function(passport) {

    // =====================================
    // Main site after loginin =============
    // =====================================
    router.get('/', isLoggedIn, function(req, res) {
        res.render('loginArea/loginIndex', {
            user: req.user
        });
    });

    // =====================================
    // Device Managment =============
    // =====================================
    router.get('/deviceManagement', isLoggedIn, function(req, res) {
        var users = new User();
        var template = new Templates();
        var remote = new Remote();

        async.series([
                function(done) {
                    if (req.user.role == 1) {
                        db.query('SELECT * FROM Devices', function(err, devices) {
                            if (err) return err;
                            done(null, devices);
                        });
                    } else {
                        db.query('SELECT * FROM Devices INNER JOIN UserToDevice ON UserToDevice.DeviceID=Devices.ID AND UserToDevice.UserID = ?', [req.user.ID],
                            function(err, devices) {
                                if (err) return err;
                                done(null, devices);
                            });
                    }
                },
            ],
            function(err, result) {
                for (var i = 0; i < result[0].length; i++)
                    remote.checkOnline(result[0][i].ID, result[0][i].IPAddress);
            });

        async.series([
                function(done) {
                    db.query('SELECT * FROM Users', function(err, users) {
                        if (err) return err;
                        done(null, users);
                    });
                },
                function(done) {
                    template.getAll(function(err, temp) {
                        if (err) return err;
                        done(null, temp);
                    });
                },
                function(done) {
                    if (req.user.role == 1) {
                        db.query('SELECT * FROM Devices', function(err, devices) {
                            if (err) return err;
                            done(null, devices);
                        });
                    } else {
                        db.query('SELECT * FROM Devices INNER JOIN UserToDevice ON UserToDevice.DeviceID=Devices.ID AND UserToDevice.UserID = ?', [req.user.ID],
                            function(err, devices) {
                                if (err) return err;
                                done(null, devices);
                            });
                    }
                }
            ],
            function(err, result) {
                res.render('loginArea/devices', {
                    devicesList: result[2],
                    userList: result[0],
                    TempList: result[1]
                });
            });
    });

    router.get('/deviceManagement/changeAdmins', isLoggedIn, function(req, res) {
        var deviceID = req.query.deviceID;
        if (!deviceID) {
            res.redirect('loginArea/deviceManagement');
        } else {
            var devices = new Devices();
            var users = new User();

            var finishRequest = function(devices, admins, users) {
                res.render('loginArea/changeAdmins', {
                    device: devices,
                    adminList: admins,
                    userList: users
                });
            }

            var q1 = function(cb) {
                db.query('SELECT * FROM Devices WHERE ID = ?', [deviceID], function(err, devices) {
                    if (err) return cb(err);
                    cb(null, devices);
                });
            }

            var q2 = function(cb) {
                db.query('SELECT * FROM UserToDevice WHERE DeviceID = ?', [deviceID], function(err, users) {
                    if (err) return cb(err);
                    cb(null, users);
                });
            }

            var q3 = function(cb) {
                db.query('SELECT * FROM Users ', function(err, users) {
                    if (err) return cb(err);
                    cb(null, users);
                });
            }

            //Standard nested callbacks
            q1(function(err, devices) {
                if (err) return;
                q2(function(err, admins) {
                    if (err) return;
                    q3(function(err, users) {
                        if (err) return;
                        finishRequest(devices, admins, users);
                    });
                });
            });
        }
    });

    router.post('/deviceManagement/addDevice', function(req, res, next) {
        var name = req.body.name;
        var location = req.body.location;
        var ip = req.body.ip;
        var deviceAdmin = req.body.deviceAdmin;
        var devices = new Devices();
        devices.AddDevice(name, location, ip, deviceAdmin);
        res.send('OK');
    });

    router.post('/deviceManagement/deleteDevice', isLoggedIn, function(req, res) {
        var deviceID = req.body.deviceID;
        var devices = new Devices();
        devices.deleteDevice(deviceID);
        res.send('OK');
    });

    router.post('/deviceManagement/addAdmin', isLoggedIn, function(req, res) {
        var deviceID = req.body.deviceID;
        var newAdminID = req.body.newAdminID;
        var devices = new Devices();
        devices.addNewAdmin(deviceID, newAdminID, res);
    });

    router.post('/deviceManagement/removeAdmin', isLoggedIn, function(req, res) {
        var deviceID = req.body.deviceID;
        var adminID = req.body.adminID;
        var devices = new Devices();
        devices.removeAdmin(deviceID, adminID);
        res.send('OK');
    });

    router.post('/deviceManagement/changeIP', isLoggedIn, function(req, res) {
        var deviceID = req.body.deviceID;
        var ipAddress = req.body.ipAddress;
        var devices = new Devices();
        devices.changeIP(deviceID, ipAddress, res);
    });

    router.post('/deviceManagement/checkOnline', isLoggedIn, function(req, res) {
        var deviceID = req.body.deviceID;
        var ip = req.body.ip;
        var remote = new Remote();
        remote.checkOnline(deviceID, ip);
        remote.getStatus(deviceID, res);
    });

    // moduly dot. breaking news
    router.post('/deviceManagement/getBreakingNews', isLoggedIn, function(req, res) {
        var deviceID = req.body.deviceID;
        var breakingNewsID = req.body.breakingNewsID;
        var bn = new BreakingNews();
        bn.getBreakingNews(deviceID, null, function(err, breakingNews) {
            if (err) return next(err);
            if (breakingNews.length != 0) {
                res.send([breakingNews[0].Content, breakingNews[0].Category, breakingNews[0].StartTime, breakingNews[0].EndTime]);
            } else
                res.send('none');
        });
    });

    router.post('/deviceManagement/changeBreakingNews', isLoggedIn, function(req, res) {
        var deviceID = req.body.deviceID;
        var IDBreakingNews = req.body.IDBreakingNews;
        var ContentBreakingNews = req.body.ContentBreakingNews;
        var CategoryBreakingNews = req.body.CategoryBreakingNews;
        var StartTime = req.body.StartTime;
        var EndTime = req.body.EndTime;
        var bn = new BreakingNews();
        bn.changeBreakingNews(deviceID, IDBreakingNews, ContentBreakingNews, CategoryBreakingNews, StartTime, EndTime);
        res.send('OK');
    });

    router.post('/deviceManagement/removeBreakingNewsFromDevice', isLoggedIn, function(req, res) {
        var deviceID = req.body.deviceID;
        var bn = new BreakingNews();
        bn.removeBreakingNewsFromDevice(deviceID);
        res.send('OK');
    });

    router.post('/deviceManagement/removeBreakingNewsFromDB', isLoggedIn, function(req, res) {
        var breakingNewsID = req.body.breakingNewsID;
        var bn = new BreakingNews();
        bn.removeBreakingNewsFromDB(breakingNewsID);
        res.send('OK');
    });

    router.get('/deviceManagement/changeBreakingNews', isLoggedIn, function(req, res) {
        var deviceID = req.query.deviceID;
        var bn = new BreakingNews();
        var finishRequest = function(devices, breakingNews) {
            res.render('loginArea/changeBreakingNews', {
                device: devices,
                breakingNewsList: breakingNews
            });
        }

        var q1 = function(cb) {
            if (!deviceID) {
                db.query('SELECT * FROM Devices ', [deviceID], function(err, device) {
                    if (err) return cb(err);
                    cb(null, device);
                });
            } else {
                db.query('SELECT * FROM Devices WHERE ID = ? ', [deviceID], function(err, device) {
                    if (err) return cb(err);
                    cb(null, device);
                });
            }
        }

        var q2 = function(cb) {
            db.query('SELECT * FROM BreakingNews ORDER BY StartTime DESC', function(err, breakingNews) {
                if (err) return cb(err);
                cb(null, breakingNews);
            });
        }

        //Standard nested callbacks
        q1(function(err, devices) {
            if (err) return;
            q2(function(err, breakingNews) {
                if (err) return;
                finishRequest(devices, breakingNews);
            });
        });
    });

    router.post('/deviceManagement/addNewBreakingNews', isLoggedIn, function(req, res) {
        var breakingNewsContent = req.body.breakingNewsContent;
        var breakingNewsCategory = req.body.breakingNewsCategory;
        var devices = new Devices();
        bn.addNewBreakingNews(breakingNewsContent, breakingNewsCategory);
        res.send('OK');
    });

    router.post('/deviceManagement/getAdminsNameByDeviceID', isLoggedIn, function(req, res) {
        var deviceID = req.body.deviceID;
        var devices = new Devices();
        devices.getAdminsName(deviceID, null, function(err, admins) {
            if (err) return next(err);
            var adminsArray = admins.map(function(it) {
                return it.Login
            });
            res.send(adminsArray);
        });
    });

    router.post('/deviceManagement/getAdminNameByUserID', isLoggedIn, function(req, res) {
        var userID = req.body.userID;
        var users = new User();
        users.getNameByID(userID, null, function(err, admin) {
            if (err) return err;
            res.send(admin[0].Login);
        });
    });

    router.post('/deviceManagement/getStatusCode', isLoggedIn, function(req, res) {
        var statusID = req.body.statusID;
        var devices = new Devices();
        devices.getStatusCode(statusID, null, function(err, device) {
            if (err) return next(err);
            res.send(device[0].CodeStatus + ': ' + device[0].Description);
        });
    });

    router.post('/deviceManagement/getTemplateName', isLoggedIn, function(req, res) {
        var deviceID = req.body.deviceID;
        var temp = new Templates();
        temp.getTemplateName(deviceID, null, function(err, templates) {
            if (err) return next(err);
            var templatesArray = templates.map(function(it) {
                return it.Name
            });
            res.send(templatesArray);
        });
    });

    router.post('/deviceManagement/updateTemplateInDevice', isLoggedIn, function(req, res) {
        var DeviceID = req.body.deviceID;
        var TemplateID = req.body.Temp;
        var template = new Templates();
        template.updateTemplateOnDevice(DeviceID, TemplateID);
        res.send("OK");
    });

    // =====================================c
    // Change Password =====================
    // =====================================
    router.get('/changePassword', isLoggedIn, function(req, res) {
        res.render('loginArea/changePassword', {
            loginUser: req.user,
        });
    });

    router.post('/changePassword', isLoggedIn, function(req, res) {
        var userID = req.body.userID;
        var password = req.body.password;
        var users = new User();
        users.resetPassword(userID, password);
        res.send('OK');
    });

    // =====================================
    // Users Managment ======================
    // =====================================
    router.get('/users', isLoggedIn, function(req, res) {
        var users = new User();
        users.getAll(function(err, result) {
            if (err) return next(err);
            res.render('loginArea/users', {
                usersList: result,
                loginUser: req.user,
            });
        });
    });

    router.post('/users/changeRole', isLoggedIn, function(req, res) {
        var userID = req.body.userID;
        var roleValue = req.body.roleValue;
        var users = new User();
        users.changeRole(userID, roleValue);
        res.send('OK');
    });

    router.post('/users/blockUser', isLoggedIn, function(req, res) {
        var userID = req.body.userID;
        var blockVal = req.body.blockVal;
        var users = new User();
        users.blockUser(userID, blockVal);
        res.send('OK');
    });

    router.post('/users/resetPassword', isLoggedIn, function(req, res) {
        var userID = req.body.userID;
        var passVal = req.body.passVal;
        var users = new User();
        users.resetPassword(userID, passVal);
        res.send('OK');
    });

    router.post('/users/deleteUser', isLoggedIn, function(req, res) {
        var userID = req.body.userID;
        var users = new User();
        users.deleteUser(userID);
        res.send('OK');
    });

    // process the add user
    router.post('/users/addUser', function(req, res, next) {
        passport.authenticate('local-addUser', function(err, success, info) {
            if (err) {
                return next(err);
            }
            var msg = req.flash('message').toString();
            return res.send({
                data: 'OK',
                msg: msg,
                success: success
            });
        })(req, res, next);
    });

    // =====================================
    // Upload file =========================
    // =====================================
    router.get('/FilesManagment/upload', isLoggedIn, function(req, res, next) {
        var template = new Templates();
        template.drawFile(function(err, files) {

            res.render('loginArea/uploadFile', {
                Files: files
            });
        });
    });

    router.post('/FilesManagment/upload', isLoggedIn, function(req, res) {
        var test;
        var path = './public/uploads/';
        var temp;

        var files = new Files();
        var template = new Templates();

        var storage = multer.diskStorage({
            destination: function(req, file, callback) {
                callback(null, path);
            },
            filename: function(req, file, callback) {
                callback(null, file.originalname);
                //console.log(file.originalname.toLowerCase());
                temp = file;
            }
        });

        var upload = multer({
            storage: storage
        }).single('tak');



        upload(req, res, function(err) {
            files.addFile(temp);

        });
            template.drawFile(function(err, files) {
            //console.log(files);
            res.render('loginArea/uploadFile', {
                Files: files
            });
          });

    });
    // =====================================
    // Delete file =========================
    // =====================================
    router.post('/FilesManagment/delete', isLoggedIn, function(req, res) {
        var FileName = req.body.FileName;
        var FileId = req.body.FileId;
        var file = new Files();
        var template = new Templates();

        file.deleteFile(FileName, FileId);
        template.drawFile(function(err, files) {

            res.send("OK");
        });
    });

    // =====================================
    // Template Managment ==================
    // =====================================
    router.get('/TemplateManagment', isLoggedIn, function(req, res) {
        var template = new Templates();
        template.getAll(function(err, result) {
            if (err) return next(err);
            var list = [];
            for(var i = 0; i < result.length; i++)
            {
              list.push(result[i].Name);
            }
            res.render('loginArea/ManageTemplate', {
                templateList: result,
                templateNames: list
            });

        });
    });

    router.post('/TemplateManagment', isLoggedIn, function(req, res) {
        NAME = req.body.templateName;
        time = req.body.templateCreation;
        OneScreen = req.body.OneScreen;
        var template = new Templates();
        template.createTemplate(NAME, time, OneScreen);
        res.redirect("/loginArea/TemplateManagment");
    });

    router.post('/TemplateManagment/DeleteTemplate', isLoggedIn, function(req, res) {
        var templateID = req.body.templateID;
        var template = new Templates();
        template.deleteTemplate(templateID);
        res.send('OK');
    });

    router.post('/TemplateManagment/checkName', isLoggedIn, function(req, res) {
        var templateName = req.body.templateName;
        var template = new Templates();
        template.getAll(function(err, templates){

        res.send(templates);
        });
    });


    router.get('/TemplateManagment/EditTemplate', isLoggedIn, function(req, res) {
        var DateFromDB;
        var DateID;
        var Files;
        var OneScreen;
        var template = new Templates();

        template.createJSON(function(err, files) {
            var NAME = req.query.title;
            Files = files;
            if (err) return cb(err);

            template.staredTemplate(NAME, function(err, dateFromDB, ID, oneScreen, StartTime, EndTime) {
                template.DisplayFilesFromScreen1(ID, function(err, DisplayFiles1) {
                    var temp;
                    var suma1 = 0;
                    for (var i = 0; i < DisplayFiles1.length; i++) {
                        temp = DisplayFiles1[i].FileDuration.split(":");
                        DisplayFiles1[i].FileDuration = 3600 * temp[0] + 60 * temp[1] + 1 * temp[2];
                        suma1 += DisplayFiles1[i].FileDuration;

                    }
                    template.DisplayFilesFromScreen2(ID, function(err, DisplayFiles2) {
                        var temp2;
                        var suma2 = 0;
                        for (var i = 0; i < DisplayFiles2.length; i++) {
                            temp2 = DisplayFiles2[i].FileDuration.split(":");
                            DisplayFiles2[i].FileDuration = 3600 * temp2[0] + 60 * temp2[1] + 1 * temp2[2];
                            suma2 += DisplayFiles2[i].FileDuration;

                        }

                        //console.log(oneScreen);
                        DateFromDB = dateFromDB;
                        DateID = ID
                        res.render('loginArea/EditTemplate', {
                            files: Files,
                            Name: NAME,
                            date1: DateFromDB,
                            Id: DateID,
                            DisplayFiles1: DisplayFiles1,
                            DisplayFiles2: DisplayFiles2,
                            suma1: suma1,
                            suma2: suma2,
                            OneScreen: oneScreen,
                            StartTime: StartTime,
                            EndTime: EndTime
                        });
                    });

                });
            });
        });
    });
    router.post('/TemplateManagment/EditTemplate', isLoggedIn, function(req, res) {
        var TempID;
        var Files;
        var DateID;
        var OneScreen;
        var NAME = req.body.title;
        var DataBegin = req.body.date_begin;
        var DataEnd = req.body.date_end;
        var Author = req.body.author
        var template = new Templates();
        //by trybiło odkomentuj

        template.drawFile(function(err, files) {
            Files = files;
            if (err) return cb(err);
            template.TemplateInfo(NAME, function(err, tempID) {
                TempID = tempID;

                template.TemplateUpdate(NAME, DataBegin, DataEnd, Author);
                var ekran1 = req.body.ekran_1;
                var ekran2 = req.body.ekran_2;
                //template.fileTotemplate(TempID, ekran1, ekran2, time1, time2, function() {});

                template.staredTemplate(NAME, function(dateFromDB, ID, oneScreen) {
                    DateFromDB = dateFromDB;
                    Id = ID;
                    OneScreen = oneScreen;

                    res.redirect("/loginArea/TemplateManagment");
                });
            });
        });


    });


    router.post("/TemplateManagment/EditTemplate/addFileToScreen1", isLoggedIn, function(req, res) {
        var Screen1 = req.body.Screen1;
        var Time1 = req.body.Time1;
        var Title = req.body.Title;
        var IDTemp = req.body.IDTemp;
        var IDFile1 = req.body.IDFile1;
        var Duration = req.body.temp1;
        var template = new Templates();
        var Time = TranslateNumberToData(Time1);
        var Dur = TranslateNumberToData(Duration);

        template.uploadDuration(Dur, IDTemp);
        template.addFileToScreen1(Screen1, IDFile1, IDTemp, Time);
        res.send("OK");
    });

    router.post('/TemplateManagment/EditTemplate/addFileToScreen2', isLoggedIn, function(req, res) {

        var Screen2 = req.body.Screen2;
        var Time2 = req.body.Time2;
        var Title = req.body.Title;
        var IDTemp = req.body.IDTemp;
        var IDFile2 = req.body.IDFile2;
        var Duration = req.body.temp2;
        var template = new Templates();
        var Time = TranslateNumberToData(Time2);
        var Dur = TranslateNumberToData(Duration);

        template.uploadDuration(Dur, IDTemp);
        template.addFileToScreen2(Screen2, IDFile2, IDTemp, Time);

        res.send("OK");
    });




    router.post('/TemplateManagment/EditTemplate/DeleteFromScreen1', isLoggedIn, function(req, res) {
        var FileID1 = req.body.FileID1;
        var Duration1 = req.body.Duration1;
        var IDTemp = req.body.IDTemp;
        var template = new Templates();

        template.DeleteFromScreen1(FileID1);
        template.uploadDuration(Duration1, IDTemp);

        res.send("OK");

    });

    router.post('/TemplateManagment/EditTemplate/DeleteFromScreen2', isLoggedIn, function(req, res) {
        var FileID2 = req.body.FileID2;
        var Duration2 = req.body.Duration2;
        var IDTemp = req.body.IDTemp;
        var template = new Templates();

        template.DeleteFromScreen2(FileID2);
        template.uploadDuration(Duration2, IDTemp);

        res.send("OK");
    });

    return router;
};

// route middleware to make sure
function isLoggedIn(req, res, next) {
    // if user is authenticated in the session, carry on route middleware to make sure
    if (req.isAuthenticated()) {
        //res.redirect('/loginArea');
        return next();
    } else {
        // if they aren't redirect them to the home page
        res.redirect('/login');
    }
}

function TranslateNumberToData(param) {
    var h = 0;
    var m = 0;
    var s = 0;
    var time = 0;
    h = (param * 1) / 3600;
    h = Math.floor(h);
    param = (param * 1) % 3600;
    m = (param * 1) / 60;
    m = Math.floor(m);
    param = (param * 1) % 60;
    s = param;
    time = h + ":" + m + ":" + s;
    return time;
}
