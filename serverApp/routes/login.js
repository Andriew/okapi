var express = require('express');
var router = express.Router();
var mysql = require('mysql');

module.exports = function(passport) {

    // =====================================
    // LOGIN ===============================
    // =====================================
    router.get('/', function(req, res) {
        // render the page and pass in any flash data if it exists
        if (req.isAuthenticated()) {
            res.redirect('loginArea');
        } else {
            res.render('login', {
                message: req.flash('loginMessage')
            });
        }
    });

    /*
        // process the login form
        router.post('/', function(req, res, next) {
            passport.authenticate('local-login', function(err, user, info) {
                if (err) {
                    return next(err);
                }
                // Redirect if it fails
                if (!user) {
                    return res.redirect('login');
                }
                req.logIn(user,
                    function(err) {
                        if (err) {
                            return next(err);
                        }
                        // Redirect if it succeeds
                        //req.session.user = user;
                        return res.redirect('loginArea/');
                    });
            })(req, res, next);
        });
    */

    router.post('/', passport.authenticate('local-login', {
        successRedirect: 'loginArea/', // redirect to the secure profile section
        failureRedirect: 'login', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));

    /*
        router.get('loginArea/', isLoggedIn,
            function(req, res) {
                // get the user out of session and pass to template
                res.render('loginArea/loginIndex', {
                    user: req.user
                });
            });
    */

    /*
    console.log('login');
    console.log(req.session.user);
    console.log(req.isAuthenticated());
    console.log(req.user);
    console.log(req.session.passport.user);
    */

    // =====================================
    // LOGOUT ==============================
    // =====================================
    /*
        router.get('/logout', function(req, res) {
            req.logout();
            //console.log(req.session.passport.user);
            res.redirect('/');
            //res.render('login');
        });
        */
    return router;
};

// route middleware to make sure
function isLoggedIn(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        res.redirect('/loginArea');
        //return next();
    } else {
        // if they aren't redirect them to the home page
        res.redirect('/');
    }
}
